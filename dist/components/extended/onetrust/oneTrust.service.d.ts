import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
export interface Purpose {
    Id: string;
    text: string;
    status: boolean;
    required: boolean;
}
export declare class OneTrustService {
    private _http;
    private registroApiUrl;
    private revocacionURL;
    private consultaUrl;
    private apikey;
    constructor(_http: HttpClient);
    registrarConsentimiento(identifier: any, requestInformation: any, purposes: any, test: any): Observable<Response>;
    consultaOpcionesAceptadasPorUsuario(identifier: any): Observable<Response>;
    revocarConsentimiento(identifier: any, purpose: any): Observable<Response>;
}
