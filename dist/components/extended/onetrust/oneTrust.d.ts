import { EventEmitter } from '@angular/core';
import { Purpose, OneTrustService } from './onetrust.service';
export declare class MapfreOneTrust {
    private otService;
    private rgpd;
    title: string;
    text: string;
    purposes: Purpose[];
    textButton: string;
    identifier: string;
    requestInformation: string;
    test: boolean;
    onetrustResult: EventEmitter<Object>;
    private showText;
    constructor(otService: OneTrustService);
    ngAfterViewInit(): void;
    stringControl(texto: any): any;
    enviarForm(): void;
    isButtonDisabled(): boolean;
    handleChange(event: any, opcion: any): void;
    private controlObj;
    showTextComplete(elemento: any): any;
}
