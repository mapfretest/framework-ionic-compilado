import { EventEmitter } from '@angular/core';
import { FabContainer } from 'ionic-angular';
export interface SubFab {
    name: string;
    icon: string;
    text?: string;
    disabled?: boolean;
    link: string;
}
export declare class MapfreFab {
    constructor();
    mostrarBackdrop: boolean;
    subFabs: SubFab[];
    name: string;
    icon: string;
    side: string;
    positionLabel: boolean;
    right: boolean;
    top: boolean;
    disabled: boolean;
    link: string;
    mClick: EventEmitter<Event>;
    ngOnInit(): void;
    doMapfreClick(event: Event, link: string, fab?: FabContainer, disabled?: boolean): void;
    doBackDropClick(fab: FabContainer): void;
}
