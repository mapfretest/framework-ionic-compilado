import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { HttpClient } from '@angular/common/http';
export interface Purpose {
    Id: string;
    text: string;
    status: boolean;
    required: boolean;
}
export declare class UploaderService {
    private _http;
    constructor(_http: HttpClient);
    getPresignedUrl(params: any): Observable<any>;
    postFileToS3(url: any, documents: any): Observable<any>;
    deleteFile(params: any): Observable<any>;
    deleteFileSuffixPrefix(params: any): Observable<any>;
    private handleError;
}
