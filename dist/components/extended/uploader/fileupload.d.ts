export interface FileUpload {
    name: string;
    size: number;
    type: string;
    content: any;
}
