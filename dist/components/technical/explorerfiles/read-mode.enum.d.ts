export declare enum ReadMode {
    arrayBuffer = 0,
    binaryString = 1,
    dataURL = 2,
    text = 3
}
